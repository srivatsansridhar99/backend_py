# Import required libraries 
from flask import Flask,request
from flask_mysqldb import MySQL

# Create Flask class instance 
app = Flask(__name__)

# Configure MySQL server credentials
app.config['MYSQL_HOST'] = "localhost"
app.config['MYSQL_USER'] = "root"
app.config['MYSQL_PASSWORD'] = "abcd1234"
app.config['MYSQL_DB'] = "kilofarm"
mysql = MySQL(app)

# Create a route to login 
@app.route('/login',methods=['GET'])
def login():
    # Obtain form fields from server 
    userDetails = request.args
    username = userDetails.get('username')
    password = userDetails.get('password')
    cur = mysql.connection.cursor()
    # Perform MySQL query to check if given username and password exists in table 
    resultValue = cur.execute("SELECT * FROM users WHERE id=%s AND password=%s",(username, password))
    cur.close()
    if(resultValue > 0):
        return "You have logged in"
    else:
        return "Invalid credentials"

# Create a route to sign up  
@app.route('/signup',methods=['GET', 'POST'])
def post():
    if request.method == "POST":
        # Obtain the form fields from server 
        userDetails = request.form.get
        username = userDetails('userName', False)
        password = userDetails('passWord',False) 
        phone_number = userDetails('phoneNo', False)
        dob = userDetails('doB', False)
        cur = mysql.connection.cursor()
        # Performs MySQL query to check if given username and password are already in the table 
        cur.execute("SELECT * FROM users WHERE phoneNumber=%s",(phone_number,))
        cur.close()
        cur = mysql.connection.cursor()
        # Performs the MySQL query to add given password, phone number and date of birth to the table 
        cur.execute("INSERT INTO users(phoneNumber,password,dob) VALUES(%s, %s, %s)", (phone_number, password, dob))
        mysql.connection.commit()
        cur.close()
        return "You have successfully signed up!"

# Run the Flask app
if __name__ == '__main__':
    app.run(debug=True)
