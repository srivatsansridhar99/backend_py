# Import necessary libraries 
from flask import Flask, request, jsonify
from flask_mysqldb import MySQL

# Create a Flask class instance 
app = Flask(__name__)

# Configure MySQL server credentials 
app.config['MYSQL_HOST'] = "localhost"
app.config['MYSQL_USER'] = "root"
app.config['MYSQL_PASSWORD'] = "abcd1234"
app.config['MYSQL_DB'] = "kilofarm"
mysql = MySQL(app)

# Create a route to give add product details to the table  
@app.route('/createSKU', methods=['POST'])
def addProducts():
    # Obtain the form fields from the server 
    productDetails = request.form
    id = productDetails['id']
    sku_name = productDetails['sku_name']
    sku_category = productDetails['sku_category']
    price = productDetails['price'] 
    cur = mysql.connection.cursor()
    # Execute the insert query to insert the product details 
    cur.execute("INSERT INTO products(id, sku_name, sku_category, price) VALUES (%s, %s, %s, %s)", (id, sku_name, sku_category, price))
    mysql.connection.commit()
    cur.close()
    return "Product has been added"

# Create a route to give retrieve product details from the table  
@app.route('/product', methods=['GET'])
def get():
    if request.method == "GET":
        # Obtain the form fields from the server
        productDetails = request.form 
        id = productDetails['id']
        cur = mysql.connection.cursor()
        # Execute query to retrieve product of given id 
        cur.execute("SELECT * FROM products WHERE id=%s", (id,))
        # Fetch the records 
        resultValue = cur.fetchall()
        cur.close()
        # Display the record using a json object 
        return jsonify(data=resultValue)

# Create a route to give update product details
@app.route('/product', methods=['PUT'])
def put():
    if request.method == "PUT":
        # Obtain form fields data 
        productDetails = request.form 
        id = productDetails['id']
        sku_name = productDetails["sku_name"]
        sku_category = productDetails["sku_category"]
        price = productDetails["price"]
        cur = mysql.connection.cursor()
        # Execute MySQL query to update product fields of given id"
        cur.execute("update products set sku_name = %s, sku_category = %s, price = %s where id = %s", (sku_name, sku_category, price, id))
        count = cur.execute("select * from products where sku_name = %s", (sku_name,))
        mysql.connection.commit()
        cur.close()
        if count > 0:
            return "Update was successful"
        else:
            return "Update was not successful"

# Create a route to give delete product details
@app.route('/product', methods=["DELETE"])
def delete():
    # Obtain form fields data 
    if request.method == 'DELETE':
        productDetails = request.form 
        id = productDetails['id']
        cur = mysql.connection.cursor()
        # Execute MySQL query to delete product record of given id
        cur.execute("delete from products where id = %s", (id,))
        count = cur.execute("Select * from products where id = %s", (id,))
        mysql.connection.commit()
        cur.close()
        if count == 0:
            return "Successfully deleted"
        else:
            return "Delete was unsuccessful"

# Run the Flask app 
if __name__ == '__main__':
    app.run(debug = True)
